FROM python:3.11.1-alpine3.16@sha256:d38982aa8ea1b37b9f6da14db4796f1a74170ccd6cb3a6f709d43f765ce7a31a

COPY poetry.lock pyproject.toml /

ARG PATH="/root/.local/bin:${PATH}"
RUN wget -q -O - https://raw.githubusercontent.com/python-poetry/install.python-poetry.org/42a10434ed127a5986c3a9952c75d333ac3a1f8e/install-poetry.py | python3 - --version 1.3.2 \
 && poetry config virtualenvs.create false \
 && apk add -t build \
    cargo \
    gcc \
    libffi-dev \
    musl-dev \
    openssl-dev \
 && poetry install --no-root \
 && apk del build

WORKDIR /code
ENTRYPOINT ["ansible-lint"]
